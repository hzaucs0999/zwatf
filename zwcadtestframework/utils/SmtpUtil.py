"""
    功能:发送结果邮件

"""
from context import contextManger
from email.mime.text import MIMEText
from email.utils import formataddr
from email.header import Header
import smtplib
class smtpclient():
    def __init__(self):
        projectcfg=contextManger.getInstance().projectcfg
        mailcfg=projectcfg["mail"]
        self.to_addr=mailcfg["recivers"]
        self.sender=mailcfg["senderaddr"]
        self.content=mailcfg["content"]
        self.title=mailcfg["title"]
        if mailcfg["smtpprotocal"]=="ssl":
            self.__init_stmp_ssl(mailcfg["smtpurl"],mailcfg["smtpport"])
        self.__login(mailcfg["senderaddr"],mailcfg["senderpwd"])

    def __init_stmp_ssl(self,smtpurl,smtpport):
        self.smtpobj= smtplib.SMTP_SSL(smtpurl, smtpport)

    def __login(self,user,pwd):
        self.smtpobj.login(user,password=pwd)


    def sendmail(self,content):
        """

        :param content: 邮件内容
        :param userto: 接受对象 List
        :return:
        """
        to_addr = self.to_addr.split(",")
        message = MIMEText(content+"\n"+self.content, 'plain', 'utf-8')
        message['From'] = formataddr(["", self.sender])  # 发送者
        message['To'] =Header(",".join(to_addr))   # 接收者
        message['Subject'] = self.title
        self.smtpobj.sendmail(self.sender,to_addrs=to_addr,msg=message.as_string())

