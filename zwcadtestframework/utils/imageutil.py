"""
    功能：图片工具类
"""
import base64
class imageUtils():
    """
        功能：图片操作工具类
    """
    @classmethod
    def convertBase64(cls,imgpath):
        """
           将图片转为base64数据
        :param imgpath:
        :return:
        """
        with open(imgpath, "rb") as file:
            base64data = str(base64.b64encode(file.read()), encoding='utf-8')
        return base64data
