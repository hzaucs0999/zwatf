from decorator import tool_decorator
import pandas as pd
import random,string
import pyperclip
import re,os
class toolapiutils():

    @staticmethod
    @tool_decorator
    def readcsvLineData(path,linenum,keyorIndex,header=None):
        """
           path:csv文件路径
           linenum:行号，从0开始计数
           keyorIndex:可以按照列号取或者按照key值取
           header:为是否忽略首行,在有表头的时候忽略，无表头时不忽略,默认为忽略
        """
        dataframe=pd.read_csv(path,header=header)
        return dataframe.iloc[linenum][keyorIndex]


    @staticmethod
    @tool_decorator
    def readexcelLineData(path, linenum, keyorIndex, header=None):
        """
           path:excel文件路径
           linenum:行号，从0开始计数
           keyorIndex:可以按照列号取或者按照key值取
           header:为是否忽略首行,在有表头的时候忽略，无表头时不忽略默认为忽略
        """
        dataframe = pd.read_excel(path,header=header)
        return dataframe.iloc[linenum][keyorIndex]


    @staticmethod
    @tool_decorator
    def stradd(a,b):
        """
           字符串相加
        """
        return a+b

    @staticmethod
    @tool_decorator
    def intadd(a, b):
        """
            字符串相加
        """
        return int(a) + int(b)

    @staticmethod
    @tool_decorator
    def randomInt(start=0, end=1):
        """
           获取随机数字
        """
        return random.randint(int(start), int(end))

    @staticmethod
    @tool_decorator
    def randomstr(len=8):
        """
           获取随机字符串
        """
        return ''.join(random.sample(string.ascii_letters + string.digits, int(len)))

    @staticmethod
    def randomlist(listdata):
        """
           listdata以"/"隔开
        """
        pass

    @staticmethod
    @tool_decorator
    def getClipData(pattern,groupindex=None,index=None):
        """
           获取最后一行的数据进行正则匹配
        """
        p = re.compile(pattern)
        groups=p.findall(pyperclip.paste().split(os.linesep)[-1])
        if groups:
           if groupindex and index:
              return groups[groupindex][index]
           else:
               return groups[0]
        else:
            raise Exception("未找到匹配项")

    @staticmethod
    @tool_decorator
    def getClipDataALL(pattern, groupindex=None, index=None):
        """
           对所有数据进行正则匹配
        """
        p = re.compile(pattern)
        groups = p.findall(pyperclip.paste())
        if groups:
            if groupindex and index:
                return groups[groupindex][index]
            else:
                return groups[0]
        else:
            raise Exception("未找到匹配项")




