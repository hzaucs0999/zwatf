"""
    功能：windows相关ui操作
    author:pengchen
    time:2021/11/24
"""
import time
from pywinauto.application import Application
from decorator import ui_decorator
import os
from common.getcontext import getTestSuiteContext
import cv2

class winui():

    def __init__(self,excutepath):
        self.excutepath=excutepath
        self.applist=[]#需要运行的windows程序的路径,绝对路径
        self.buttoncache= {}

    @ui_decorator
    def openProgram(self,timeout=60000):
        """
           启动应用程序
        :return:
        """
        if not self.excutepath or not os.path.exists(self.excutepath):
            raise Exception("文件路径不存在无法打开目录")
        self.applist.append(Application(backend="uia").start(self.excutepath,timeout=int(timeout)))


    @ui_decorator
    def startnewProgram(self,excutepath,timeout=60000):
        """
           启动应用程序,不使用默认路径
        """
        if not excutepath or not os.path.exists(excutepath):
            raise Exception("文件路径不存在无法打开目录")
        self.applist.append(Application(backend="uia").start(excutepath,timeout=int(timeout)))

    @ui_decorator
    def connectProgram(self,appindex,backend="win32",timeout=60000):
        """
           连接应用程序，通过appindex来获取processid
        """
        #

        self.applist.append(Application(backend=backend).connect(self.applist[appindex].process,timeout=int(timeout)))

    @ui_decorator
    def switchDialog(self,title,appindex=-1):
        """
            切换到窗口
            #默认切换到当前目录最后一个
        """
        starttime = time.time()
        app=self.applist[int(appindex)]
        while True:
            findflag = False
            try:
                # if title=="top_window":
                #     self.current_app=app.top_window()
                #     findflag=True
                #     break
                if hasattr(app, title):
                    self.current_app = getattr(app, title)
                    if self.current_app.wrapper_object().element_info.visible:
                        findflag = True
                        self.current_app.set_focus()
                        print("已找到%s窗口"%(title))
                        break
            except Exception as e:
                print("暂未找到窗口,等待3s后继续查询")
            endtime = time.time()
            print("经过时间%f" % (endtime - starttime))
            if findflag or endtime - starttime > 60:
                break
        if not findflag:
            raise Exception("在60s时间内未找到指定窗口")



    @ui_decorator
    def findcontrol(self,title=None,control_type=None,auto_id=None,timeout=60):
        """
            寻找控件
        """
        starttime=time.time()
        control=None
        while True:
            findflag=False
            try:
                control=self.current_app.child_window(title=title,control_type=control_type,auto_id=auto_id).wrapper_object()
                findflag=True
                """判断那个存在,用那个"""
                if title:
                    key=title
                elif auto_id:
                    key=auto_id
                self.buttoncache[key]=control
            except Exception as e:
                print("暂未找到控件,等待后继续查询")
            endtime=time.time()
            print("经过时间%f"%(endtime-starttime))
            if findflag or endtime-starttime>float(timeout):
                break
        if not findflag:
            raise Exception("在60s时间内未找到指定控件")
        return control

    @ui_decorator
    def findcontrolfuzzymatch(self, bestmatch, timeout=60):
        """
           模糊查询控件
        """
        starttime = time.time()
        control = None
        while True:
            findflag = False
            try:
                if bestmatch in self.buttoncache:
                    control=self.buttoncache[bestmatch]
                    if control.element_info.visible:
                        findflag=True
                        print("已找到%s控件" % (bestmatch))
                        break
                    else:
                        print("%s控件不可见，删除缓存，重新查找")
                        self.buttoncache.pop(bestmatch)
                elif hasattr(self.current_app,bestmatch):
                    control = getattr(self.current_app,bestmatch).wrapper_object()
                    if control.element_info.visible:
                        findflag = True
                        print("已找到%s控件" % (bestmatch))
                        self.buttoncache[bestmatch]=control
                        break
                # control = self.current_app.child_window(auto_id=autoid).wrapper_object()
                # findflag = True
                # self.buttoncache.append(control)
            except Exception as e:
                print("暂未找到控件,等待后继续查询")
            endtime = time.time()
            print("经过时间%f" % (endtime - starttime))
            if findflag or endtime - starttime > float(timeout):
                break
        if not findflag:
            raise Exception("在60s时间内未找到指定控件")
        return control

    @ui_decorator
    def click(self,tag):
        control=self.buttoncache[tag]
        try:
            control.click()
        except Exception as e:
            #如果点击报错，则尝试获取控件坐标后在
            print("click方法不支持,采用invoke")
            control.invoke()

    @ui_decorator
    def menu_select(self, title, appindex=-1):
        """
            上下文菜单
        """
        app = self.applist[int(appindex)]
        if hasattr(app, 'PopupMenu'):
            getattr(app, 'PopupMenu').wrapper_object().item_by_path(title).click_input()


    @ui_decorator
    def type_keys(self,tag,value):
        # print("typekeys", self.current_app.wrapper_object().element_info)
        start=time.time()
        control = self.buttoncache[tag]
        end=time.time()
        print("花费时间%f"%(end-start))
        # control.set_focus()
        # pywinauto.keyboard.send_keys(value)
        start = time.time()
        control.type_keys(value)
        end = time.time()
        print("花费时间%f"%(end - start))

    def close(self):
        if hasattr(self,"current_app"):
            try:
              self.current_app.close()
            except:
                print("关闭失败，窗口已经关闭")

        for app in self.applist:
            app.kill()

    @ui_decorator
    def maximum_size(self):
        self.current_app.maximize()

    def screenshot(self,path):
        if hasattr(self,"current_app"):
           self.current_app.capture_as_image().save(path)
        else:
            #置顶的应用程序截图
            if len(self.applist)>0:
              self.applist[-1].top_window().capture_as_image().save(path)
            else:
                print("没有截图对象，不做截图处理")

    @ui_decorator
    def user_screen_shot(self):
        path=getTestSuiteContext().getImagePath()
        self.screenshot(path)
        getTestSuiteContext().cur_process_result.addcaptureimageshot(path)

    @ui_decorator
    def getcontrolPosition(self, tag):
        """
            获取当前控件坐标，并将坐标存入params,可以通过current_x,和current_y来调用，默认返回中心点坐标
        :param name: 控件选取字段
        :return:
        """
        control = self.buttoncache[tag]
        controlrect = control.rectangle()
        control_middle_x = controlrect.left + (controlrect.right - controlrect.left) / 2
        control_middle_y = controlrect.top + (controlrect.bottom - controlrect.top) / 2
        getTestSuiteContext().params["current_x"]=control_middle_x
        getTestSuiteContext().params["current_y"] = control_middle_y


    def _capture_control_as_image(self, control,path):
        """
           截取当前控件为图片
        """
        control.capture_as_image().save(path)

    @ui_decorator
    def getControlPosbyImagePattern(self,controlimagepath):
        """
           通过控件的图片在page图片上进行比对，找到后通过鼠标进行点击,取中点
        :param controlimagepath: 不支持中文路径
        :return:
        """
        #截取当前窗口图片
        #如果未找到，则找60s


        template = cv2.imread(controlimagepath, 0)
        w, h = template.shape[::-1]
        dialogRect = self.current_app.wrapper_object().rectangle()
        maxval=0.7
        starttime=time.time()
        while True:
            path = "temp1.jpg"
            self._capture_control_as_image(self.current_app, "temp1.jpg")
            img_rgb = cv2.imread(path, 0)
            res = cv2.matchTemplate(img_rgb, template, cv2.TM_CCOEFF_NORMED)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            endtime=time.time()
            print("maxval",maxval)
            print("max_val",max_val)
            if maxval>max_val:
               if (endtime-starttime)>60:
                   break
            else:
                break
            #间隔3s继续执行
            time.sleep(3)

        #获取中点坐标
        current_x=(w-max_loc[0])/2+dialogRect.left
        current_y=h/2+dialogRect.top+max_loc[1]
        print(current_x, current_y)
        getTestSuiteContext().params["current_x"] = current_x
        getTestSuiteContext().params["current_y"] = current_y

    @ui_decorator
    def print_control_identifiers(self,path=None):
        """
            打印当前程序的current_app的所有控件信息
        """
        self.current_app.print_control_identifiers(filename=path)



    @ui_decorator
    def getwindowText(self,tag,outputname):
        control = self.buttoncache[tag]
        getTestSuiteContext().params[outputname] = control.window_text()













