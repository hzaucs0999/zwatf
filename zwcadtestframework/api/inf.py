"""
   功能:后端接口类封装实现

"""
import requests
from common.getcontext import getContext
from decorator import inf_decorator

class InfRequest():
    #
    def __init__(self):
        # 初始化实体类
        self.cookies = None
        self.domain = getContext().projectcfg.environment.domain
        self.protocal =getContext().projectcfg.environment.protocal

    @inf_decorator
    def excute(self, method, api_url, headers, files, json, data, params, responsetype):
        """
            method:执行方式 get，post等
            api_url:请求路径
            headers:请求头
            files:请求中需要传输的文件
            json:json数据
            data:键值对数据
            params:url后数据
            reponsetype:响应数据
        """
        url=self.protocal+":"+"//"+self.domain+api_url
        result = requests.request(method, url=url, params=params, data=data, headers=headers, files=files,
                                  json=json,cookies=self.cookies)
        if not self.cookies:
            self.cookies=result.cookies
        if responsetype=="html":
            result.encoding=result.apparent_encoding
            return {"type":"html","statu_code":result.status_code,"content":result.text}
        elif responsetype=="json":
            return {"type":"json","statu_code":result.status_code,"content":result.json()}
        elif responsetype=="file":
            return {"type":"file","statu_code":result.status_code,"content":result.content}
        else:
            raise Exception("暂不支持的返回值类型")
