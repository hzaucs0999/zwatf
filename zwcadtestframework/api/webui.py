"""
   功能:api相关
"""
from playwright.sync_api import sync_playwright,Error,Page,Frame
from common.getcontext import *
import time
from decorator import ui_decorator
import cv2,os
import json
pathmap={"chrome":"chromebroswerpath","firefox":"firefoxbroswerpath","webkit":"webkitbroswerpath"}

class webui():

    def __init__(self, browertype, devices=None, headless=False):
        self.page=None
        self.playwright = sync_playwright().start()
        self.headless=headless
        if browertype == "chrome":
            self.type = self.playwright.chromium
        elif browertype == "firefox":
            self.type = self.playwright.firefox
        elif browertype == "webkit":
            self.type = self.playwright.webkit
            self.playwright.devices = devices

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


    def _update_busy_time(self, event=None) -> None:
        if isinstance(event, Page) or isinstance(event, Frame):
           self._register_busy_time(event)
           self.last_busy_time = time.time()

    def _register_busy_time(self,obj):
        obj.on('domcontentloaded', self._update_busy_time)
        obj.on('download', self._update_busy_time)
        obj.on('filechooser', self._update_busy_time)
        obj.on('frameattached', self._update_busy_time)
        obj.on('framedetached', self._update_busy_time)
        obj.on('framenavigated', self._update_busy_time)
        obj.on('load', self._update_busy_time)
        obj.on('pageerror', self._update_busy_time)
        obj.on('popup', self._update_busy_time)
        obj.on('request', self._update_busy_time)
        obj.on('requestfailed', self._update_busy_time)
        obj.on('requestfinished', self._update_busy_time)
        obj.on('response', self._update_busy_time)

    @ui_decorator
    def openbrowser(self,url=None):
        """
           打开浏览器
        """
        self._lanuch(url)
        self.wait_until_idle()

    def _lanuch(self, url=None):
        """
           首先判断默认路径下是否有浏览器，若存在则调用该浏览器
           若不存在，则调用配置文件中的浏览器,若都不存在，则报错
        """
        print(self.type.executable_path)
        if os.path.exists(self.type.executable_path):
            self.browser = self.type.launch(headless=self.headless,timeout=1200000)
        else:
            projectcfg=getContext().projectcfg
            browserpath=projectcfg.getBrowsetpath()
            if os.path.exists(browserpath):
               self.browser=self.type.launch(executable_path=browserpath,headless=self.headless,timeout=1200000)
            else:
                raise Exception("浏览器地址不存在")
        self.context = self.browser.new_context(no_viewport=True)
        self.page = self.context.new_page()
        self._register_busy_time(self.page)
        if url:
            self.page.goto(url)

    @ui_decorator
    def goto(self,url):
        """
           访问url
        """
        if url:
            self.page.goto(url)
        else:
            raise Exception("要打开的网页不能为空")


    @ui_decorator
    def click(self, selector):
        """
           点击控件
        """
        control=self.getSelector(selector)
        control.click()
        self.wait_until_idle()

    @ui_decorator
    def fill(self, selector,value):
        """
          在控件中输入数据
        """
        control = self.getSelector(selector)
        control.fill(value)
        self.wait_until_idle()


    def close(self):
        if not self.page:
           self.headless=True
           self._lanuch()
        self.page.close()
        self.context.close()
        self.browser.close()
        self.playwright.stop()

    def getSelector(self,selector):
        """
           获取当前selector
        :param selector:
        :return:
        """
        #1.首先取selectormap下寻找
        selectormap = getContext().selector
        path=None
        if selector in selectormap:
           path=selectormap[selector]["path"]
        #2.不能完全匹配，则通过模糊匹配的方式进行匹配
        else:
            for key in selectormap:
                if selector in key:
                    path=selectormap[key]["path"]
        #3.如果还找不到，直接页面查找
        if not path:
            path=selector
        if "=>" in path:
            #若包含则认为含有frame,切割后进行拆线呢
           pathlist=path.split("=>")
           idx=0
           control=self.page
           for curpath in pathlist:
               if idx==len(pathlist)-1:
                   control=control.query_selector(curpath)
               else:
                   control=control.query_selector(curpath).content_frame()
               idx=idx+1
           return control
        control=self.page.query_selector(path)
        return control

    def wait_until_idle(self):
        while time.time()-self.last_busy_time<1:
            self.page.wait_for_timeout(100)

    @ui_decorator
    def switchPage(self,newPagetitleName):
        """
           切换到新的页签
        :param newPageName:
        :return:
        """
        cur_pages=self.context.pages
        for page in cur_pages:
            if page.title()==newPagetitleName:
                self.page=page
        self._register_busy_time(self.page)
        self.wait_until_idle()

    @ui_decorator
    def upload(self,selector,filepath):
        """
           上传文件
        :param selector:
        :param filepath:
        :return:
        """
        control=self.getSelector(selector)
        with self.page.expect_file_chooser() as fc_info:
            control.click()
        file_chooser = fc_info.value
        file_chooser.set_files(filepath)
        
    @ui_decorator
    def getHtmlvalue(self,outputname,selector,property=None):
        """
           获取页面上控件的属性数据
        """
        control=self.getSelector(selector)
        expression="node=>node."+str(property)
        value=control.evaluate(expression)
        getTestSuiteContext().params[outputname]=value

    @ui_decorator
    def getControlPosbyImagePattern(self,controlimagepath):
        """
           通过控件的图片在page图片上进行比对，找到后通过鼠标进行点击,取中点
        :param controlimagepath: 只支持英文路径
        :return:
        """
        #截取当前窗口图片

        template = cv2.imread(controlimagepath, 0)
        w, h = template.shape[::-1]
        maxval=0
        starttime = time.time()
        while True:
            self.page.screenshot("temp1.jpg")
            img_rgb = cv2.imread("temp1.jpg", 0)
            res = cv2.matchTemplate(img_rgb, template, cv2.TM_CCOEFF_NORMED)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            endtime = time.time()
            #获取中点坐标
            if maxval > max_val:
                if (endtime - starttime) > 60:
                    break
            else:
                break
            # 间隔3s继续执行
            time.sleep(3)
        current_x=(w-max_loc[0])/2
        current_y=h/2
        #print(current_x,current_y)
        getTestSuiteContext().params["current_x"] = current_x
        getTestSuiteContext().params["current_y"] = current_y

    @ui_decorator
    def user_screen_shot(self):
        """
           用户截图
        """
        path = getTestSuiteContext().getImagePath()
        self.page.screenshot(path=path)
        getTestSuiteContext().cur_process_result.addcaptureimageshot(path)


    @ui_decorator
    def evaluateJS(self,jsScript,value=None,outputparamname=None):
        """
            执行js脚本

        """
        #判断是否是文件
        if os.path.isfile(jsScript):
            #读取脚本内容
            file=open(jsScript,"r",encoding="utf-8")
            scriptdata=file.read()

        def return_json(jsonstr):
            # 处理csv中的单引号
            try:
                json_object = json.loads(jsonstr)
            except ValueError as e:
                return jsonstr
            return json_object
        if value:
            result=self.page.evaluate(jsScript,return_json(value))
        else:
            result = self.page.evaluate(jsScript)
        if outputparamname:
            getTestSuiteContext().params[outputparamname]=result