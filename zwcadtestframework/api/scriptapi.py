"""
   脚本执行api,
"""

from common.getcontext import getTestSuiteContext,getContext
from decorator import script_decorator


class scriptApi():



    @script_decorator
    def excute(self,scriptpath):
        """
           执行函数
        """
        #加载脚本
        module=getTestSuiteContext().loadPyModule(scriptpath)
        #执行module方法固定为传入当前环境变量
        if hasattr(module,"excute"):
            module.excute(getContext(),getTestSuiteContext())
        else:
            raise Exception("无excute执行函数")


