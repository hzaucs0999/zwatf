"""
      功能:websocket-client实现类
      支持多个，新创建的下标为0,以此类推
"""
from websocket import create_connection
from decorator import ws_decorator
class websocketclientManager(object):
    class websocketclient():
        def __init__(self,uri):
            self.uri=uri

        def connect(self):
            """
              链接客户端
            """
            self.client= create_connection(self.uri)
        def sendmsg(self,msg):
            self.client.send(msg)

        def recivemsg(self):
            return self.client.recv()

        def close(self):
            self.client.close()



    def __init__(self):
        self._clientLst = []
        
    @ws_decorator
    def createConnection(self,uri):
        wsclient=websocketclientManager.websocketclient(uri)
        wsclient.connect()
        self._clientLst.append(wsclient)

    @ws_decorator
    def sendMsg(self,index,msg):
        self._clientLst[int(index)].sendmsg(msg)

    @ws_decorator
    def reciveMsg(self,index):
        return self._clientLst[int(index)].recivemsg()

    def close(self,index):
        self._clientLst[int(index)].close()

    def closeAll(self):
        for client in self._clientLst:
            client.close()












