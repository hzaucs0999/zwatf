"""
    功能：键盘操作，通过pyautogui
    time:2021/12/06
"""

import pyautogui
from decorator import mousekeyboard_decorator

class KeyBoard():
    @mousekeyboard_decorator
    def press(self, key):
        """
           按键['\t', '\n', '\r', ' ', '!', '"', '#', '$', '%', '&',
            "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1',
            '2', '3', '4', '5', '6', '7', '8', '9', ':', ';',
            '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_',
            '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}',
            '~', 'accept', 'add', 'alt', 'altleft', 'altright', 'apps',
             'backspace', 'browserback', 'browserfavorites', 'browserforward',
              'browserhome', 'browserrefresh', 'browsersearch', 'browserstop',
              'capslock', 'clear', 'convert', 'ctrl', 'ctrlleft', 'ctrlright',
              'decimal', 'del', 'delete', 'divide', 'down', 'end', 'enter', 'esc',
              'escape', 'execute', 'f1', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15',
              'f16', 'f17', 'f18', 'f19', 'f2', 'f20', 'f21', 'f22', 'f23', 'f24',
               'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'final', 'fn', 'hanguel',
                'hangul', 'hanja', 'help', 'home', 'insert', 'junja', 'kana',
                'kanji', 'launchapp1', 'launchapp2', 'launchmail', 'launchmediaselect',
                'left', 'modechange', 'multiply', 'nexttrack', 'nonconvert', 'num0',
                'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'num7', 'num8', 'num9',
                'numlock', 'pagedown', 'pageup', 'pause', 'pgdn', 'pgup', 'playpause',
                'prevtrack', 'print', 'printscreen', 'prntscrn', 'prtsc', 'prtscr', 'return',
                 'right', 'scrolllock', 'select', 'separator', 'shift', 'shiftleft',
                 'shiftright', 'sleep', 'space', 'stop', 'subtract', 'tab', 'up', 'volumedown',
                  'volumemute', 'volumeup', 'win', 'winleft', 'winright', 'yen', 'command',
                  'option', 'optionleft', 'optionright']
               通过"/"分割
        """
        pyautogui.press(key.split("/"))

    @mousekeyboard_decorator
    def hotkeys(self,hotkeys):
        """
           hotkeys,通过 '/'
        """
        args=hotkeys.split("/")
        pyautogui.hotkey(*args)

    @mousekeyboard_decorator
    def keydown(self,key):
        """
            长按按键,只支持单键
        """
        pyautogui.keyDown(key)

    @mousekeyboard_decorator
    def keyup(self,key):
        """
           松开按键，只支持单键
        """
        pyautogui.keyUp(key)

    @mousekeyboard_decorator
    def presscmdln(self,cmd):
        """
           输入命令后按回车
        """
        pyautogui.press(list(cmd))
        pyautogui.press("enter")

    @mousekeyboard_decorator
    def presscmd(self,cmd):
        """
                   输入命令后不按回车
            """
        pyautogui.press(list(cmd))






