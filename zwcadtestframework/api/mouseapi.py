"""
    鼠标操作相关，使用pyautogui
"""
import pyautogui
from decorator import mousekeyboard_decorator
from common.getcontext import getTestSuiteContext

class Mouse():


    @mousekeyboard_decorator
    def getScreenSize(self):
        """
          获取当前屏幕尺寸
          第一个为宽度，第二个为长度
        """
        getTestSuiteContext().params["current_screen_width"] = pyautogui.size()[0]
        getTestSuiteContext().params["current_screen_height"] = pyautogui.size()[1]


    @mousekeyboard_decorator
    def moveTo(self,x=None,y=None,duration=0.25):
        """
           针对屏幕而言，即x=100，y=100，duration=0.25
           等于移动到屏幕坐标100，100，在0.25s时间内
           不填入即为不移动
        """
        mo_x,mo_y=pyautogui.position()
        x= float(x) if x else mo_x
        y= float(y) if x else mo_y
        pyautogui.moveTo(float(x),float(y),duration=duration)

    @mousekeyboard_decorator
    def move(self,x=0,y=0,duration=0.25):
        """
           针对当前鼠标坐标而言，即x=100，y=100，duration=0.25，鼠标处于300，300
           等于移动到坐标400，400，在0.25s时间内默认为0 不移动
        """
        pyautogui.moveRel(float(x),float(y),duration=duration)

    @mousekeyboard_decorator
    def getMousePosition(self):
        """
           获取鼠标当前坐标，前一个值为X坐标，后一个为Y坐标
        """
        getTestSuiteContext().params["current_mouse_x"] =pyautogui.position()[0]
        getTestSuiteContext().params["current_mouse_y"] =pyautogui.position()[1]


    @mousekeyboard_decorator
    def mouseclick(self,x=None,y=None,type="single_click",button="left"):
        """
           将鼠标移动到当前x,y位置并点击
           type为点击方式，双击，单击,single_click,double_click,tripe_click
           button为点击按键，left,middle,right
        """
        x = float(x) if x else None
        y = float(y) if y else None
        if type=="single_click":
           pyautogui.click(x,y,button=button)
        elif type=="double_click":
            """双击坐标"""
            self._doubleclick(x,y,button)
        else:
            """仅支持左击"""
            if button.lower()=="left":
                pyautogui.tripleClick(x,y)
            else:
                raise Exception("右键中键暂不支持")

    def _doubleclick(self,x,y,button):
        if button=="left":
            pyautogui.doubleClick(x,y)
        elif button=="right":
            pyautogui.rightClick(x,y)
        else:
            pyautogui.middleClick(x,y)

    @mousekeyboard_decorator
    def dragTo(self,x,y,duration=0.25):
        """
         把鼠标拖拽到(100, 0)位置，点击鼠标右键
        """
        pyautogui.dragTo(float(x),float(y),duration=float(duration))

    @mousekeyboard_decorator
    def drag(self,x,y,duration=0.25):
        """
         相对于当前位置拖动鼠标，x、y可以为负数，用于选中文本
        """
        pyautogui.dragRel(float(x),float(y),duration=float(duration))

    @mousekeyboard_decorator
    def mouseDown(self,button="left"):
        """
         鼠标按下
         button="left",middle,right,默认左
        """

        pyautogui.mouseDown(button=button)

    @mousekeyboard_decorator
    def mouseUp(self,button="left",x=None,y=None):
        """
          移动到(x, y)位置，然后松开鼠标右键
        """
        pyautogui.mouseUp(button=button,x=float(x),y=float(y))

    @mousekeyboard_decorator
    def scroll(self,x):
        """
            模拟滚轮滑动,x为正则正向滚动，否则反向滚动
        """
        pyautogui.scroll(float(x))




