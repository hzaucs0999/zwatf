import os
from common.toolutil import *
import time
from apiconfig import webuiapi, winuiapi, requestsapi, wsapi, mouseapi, keyboardapi,scriptapi,toolapi
import datetime
import re
from config.projectcfg import ProjectCfg


class contextManger():
    __init__flag = True

    @classmethod
    def getInstance(cls):
        return cls._instance

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance'):
            orig = super(contextManger, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance

    def __init__(self, projectdir):
        if self.__init__flag:
            self.starttime = time.asctime(time.localtime(time.time()))
            self.projectdir = projectdir
            self.loadProjectCfg()
            self.loadInterface()
            self.loadSelector()
            self.loadtesttask()
            # 注册报告impl
            self.__init__flag = False
            # self.current_idx=self.getCurrentFileidx()
            self.current_idx=time.strftime("%Y-%m-%d %H%M%S")

    def loadProjectCfg(self):
        projectyamlpath = self.projectdir + os.sep + "project.yaml"
        self.projectcfg = ProjectCfg(getyamldata(projectyamlpath)["projectconfig"])

    def loadInterface(self):
        interfaceyamlpath = self.projectdir + os.sep + "interface.yaml"
        if os.path.exists(interfaceyamlpath):
           self.interface = getyamldata(interfaceyamlpath)["interfaces"]

    def loadSelector(self):
        selectoryamlpath = self.projectdir + os.sep + "selector.yaml"
        if os.path.exists(selectoryamlpath):
            self.selector =getyamldata(selectoryamlpath)["selectors"]

    def loadtesttask(self):
        taskpath = self.projectdir + os.sep + "entry.yaml"
        self.task = getyamldata(taskpath)

    def getResultpath(self):
        result=self.projectdir + os.sep + "result" + os.sep+self.current_idx
        return result



class TestSuiteContextManager():
    """
        测试集上下文
    """
    __apientrymap = {}
    __webui = None
    __winui = None
    __request = None
    __wsapi = None
    __keyboardapi = None
    __mouseapi = None
    __scriptapi=None

    @classmethod
    def getInstance(cls):
        return cls._instance

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance'):
            orig = super(TestSuiteContextManager, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance

    def __init__(self, testsuite):
        """
             根据测试集内容初始化上下文
        :param testsuite:
        """
        self.params = {}
        self.__name = testsuite.name
        # 生成logger对象
        keywordlist = []
        for testcase in testsuite.testcaselist:
            # 获取关键字列表
            for action in testcase.actionlist:
                keywordlist.append(action.action)
        self.register_api(keywordList=keywordlist)
        # self.current_idx = time.strftime("%Y-%m-%d %H%M%S")

    def register_api(self, keywordList):
        """
          根据keyword记载对应api和生成api对象
        :param keywordList:
        :return:
        """
        projectcfg = contextManger.getInstance().projectcfg
        for keyword in keywordList:
            if keyword in winuiapi:
                if not self.__winui:
                    winuimodule = __import__("api.winui")
                    self.__winui = winuimodule.winui.winui(projectcfg.environment.excutepath)
                self.__apientrymap[keyword] = getattr(self.__winui, winuiapi[keyword])
            if keyword in webuiapi:
                if not self.__webui:
                    webuimodule = __import__("api.webui")
                    self.__webui = webuimodule.webui.webui(projectcfg.getBrowsettype(),
                                                           headless=projectcfg.environment.headless)
                self.__apientrymap[keyword] = getattr(self.__webui, webuiapi[keyword])
            if keyword in requestsapi:
                if not self.__request:
                    requestmodule = __import__("api.inf")
                    self.__request = requestmodule.inf.InfRequest()
                self.__apientrymap[keyword] = getattr(self.__request, requestsapi[keyword])
            if keyword in wsapi:
                if not self.__wsapi:
                    wsmodule = __import__("api.wsapi")
                    self.__wsapi = wsmodule.wsapi.websocketclientManager()
                self.__apientrymap[keyword] = getattr(self.__wsapi, wsapi[keyword])
            if keyword in mouseapi:
                if not self.__mouseapi:
                    mousemodule = __import__("api.mouseapi")
                    self.__mouseapi = mousemodule.mouseapi.Mouse()
                self.__apientrymap[keyword] = getattr(self.__mouseapi, mouseapi[keyword])
            if keyword in keyboardapi:
                if not self.__keyboardapi:
                    keyboardmodule = __import__("api.keyboardapi")
                    self.__keyboardapi = keyboardmodule.keyboardapi.KeyBoard()
                self.__apientrymap[keyword] = getattr(self.__keyboardapi, keyboardapi[keyword])
            if keyword in scriptapi:
                if not self.__scriptapi:
                    scriptapimodule = __import__("api.scriptapi")
                    self.__scriptapi=scriptapimodule.scriptapi.scriptApi()
                self.__apientrymap[keyword]=getattr(self.__scriptapi,scriptapi[keyword])
            if keyword in toolapi:
                randomtoolmodule = __import__("api.datautil")
                self.__apientrymap[keyword]=getattr(randomtoolmodule.datautil.toolapiutils, toolapi[keyword])


    def getAction(self, keyword):
        if keyword in self.__apientrymap:
            return self.__apientrymap[keyword]
        else:
            return None

    def getWebUiObject(self):
        return self.__webui

    def getWinUiObject(self):
        return self.__winui

    def close(self):
        # 关闭对象，清空var,注销logger
        if self.__winui:
            self.__winui.close()
            self.__winui = None

        if self.__webui:
            self.__webui.close()
            self.__webui = None

        if self.__wsapi:
            self.__wsapi.closeAll()
            self.__wsapi = None

        self.params = {}
        self.__apientrymap = {}

    def getTestSuiteName(self):
        return self.__name

    def screenshot(self, keyword):
        # 根据关键字给对应的截图对象
        testcasename, testcasecode = self.getTestCase()
        imagepath = self.getcurpath() + os.sep + testcasename + "_" + testcasecode + "_" + str(
            int(time.time()*1000)) + ".png"
        if keyword in winuiapi:
            self.__winui.screenshot(imagepath)
        elif keyword in webuiapi:
            if hasattr(self.__webui,"page") and self.__webui.page:
               self.__webui.page.screenshot(path=imagepath)
            else:
                print("无截图对象，不做截图处理")
        else:
            # 加载imageGrab对象并截图
            pyautogui = __import__("pyautogui")
            pyautogui.screenshot(imagepath)
            #根据上一次的截图情况来进行截图

        return imagepath

    def getImagePath(self):
        testcasename, testcasecode =  self.getTestCase()
        imagepath = self.getcurpath() + os.sep + testcasename + "_" + testcasecode + "_" + str(
            int(time.time() * 1000))+"user_capture" + ".png"
        return imagepath

    @classmethod
    def closeInstance(cls):
        del cls._instance


    def loadPyModule(self,path):
        """
           加载python module
        """
        module=loadpythomodule(path)
        return module

    def loaddllmodule(self,path):
        dll=loaddllmodule(path)
        return dll

    def register_keyword(self,keywordname,function):
        if keywordname in self.__apientrymap:
            raise Exception("关键字%s已存在,请更换名字"%(keywordname))
        else:
            def third_decorator(a_func):
                def make_decorator(inputparam, var):
                    result = {}
                    result["errorimageShot"] = []
                    starttime = time.time()
                    try:
                        args=[]
                        for param in inputparam.split("|"):
                            param=replace(param,var)
                            args.append(param)
                        #根据arg
                        content=a_func(*args)
                        print("current_return",content)
                        self.params["third_return_data"]=content
                        result["excuteresult"] = "ok"
                    except Exception as e:
                        result["excuteresult"] = "pok"
                        result["errorinfo"] = e.args[0]
                    endtime = time.time()
                    result["duration"] = endtime - starttime
                    return result
                return make_decorator
            self.__apientrymap[keywordname]=third_decorator(function)

    def push_param(self,key,value):
        self.params[key]=value

    def getTestCase(self):
        return self.__cur_testcase_name, self.__cur_testcase_code

    def getTestSuitename(self):
        return self.__cur_testsuite_name

    def setTestCase(self, caseinfo):
        self.__cur_testcase_name = caseinfo[0]
        self.__cur_testcase_code = caseinfo[1]

    def setTestsuite(self, name):
        self.__cur_testsuite_name = name

    def getcurpath(self):
        parentpath=contextManger.getInstance().getResultpath()
        current_path=parentpath+os.sep+self.__cur_testsuite_name
        return current_path
