cd /d %~dp0
cd ..
set pipfile="Pipfile"
if not exist %pipfile% (
   pipenv install
)
pipenv run pip install pyinstaller
pipenv run pip install requests
pipenv run pip install playwright
pipenv run pip install pywinauto
pipenv run pip install websocket-client
pipenv run pip install pluggy
pipenv run pip install bs4
pipenv run pip install pandas
pipenv run pip install Pillow
pipenv run pip install click
pipenv run pip install pyautogui
pipenv run pip install pyyaml
pipenv run pip install opencv-python
pipenv run pyinstaller -F main.spec
