"""
    项目中使用的工具方法
"""
import yaml
import pandas as pd
import importlib
import ctypes
import re, json


def loadpythomodule(scriptpath):
    """
       加载python脚本
    """
    spec = importlib.util.spec_from_file_location('script', scriptpath)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def loaddllmodule(dllpath):
    """
       加载dll模块
    """
    dll = ctypes.windll.LoadLibrary(dllpath)
    return dll


def readcsv(path):
    """
       读取csv
    """
    dataframe = pd.read_csv(path)
    return dataframe


def getyamldata(path):
    """
       获取yamldata
    """
    with open(path, 'r', encoding='utf-8') as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)
    return cfg


def replace(arg, var):
    """
         ${参数名称}替换，只匹配单个
    :param arg:
    :param var:
    :return:
    """
    pattern = "\$\{(.*)\}"
    group = re.match(pattern, arg)
    if group:
        varname = group[1]
        value = var[varname]
        if not value:
            value = ""
        return arg.replace(group[0], str(value))
    return arg


def replaceAll(arg, var, idx):
    pattern = "\$\{([^$]*)\}"
    paramnamelst = re.findall(pattern, arg, flags=0)
    for paramname in paramnamelst:
        if paramname in var:
            arg = arg.replace('${%s}' % (paramname), var["paramname"])
        else:
            arg = arg.replace('${%s}' % (paramname), var["paramlist"][idx])
            idx = idx + 1
    return arg, idx


def is_json(jsonstr):
    # 处理csv中的单引号
    """
       判断是否是json
    """
    try:
        json_object = json.loads(jsonstr)
    except ValueError as e:
        return False
    return True


def is_number(s):
    """
       判断是否是数字
    """
    try:
        float(s)
        return True
    except ValueError:
        return False
    return False
