"""
   获取上下文，避免循环引入
"""


def getContext():
    """
       获取项目上下文
    :return:
    """
    from context import contextManger
    return contextManger.getInstance()

def getTestSuiteContext():
    """
        获取测试集上下文
    """
    from context import TestSuiteContextManager
    return TestSuiteContextManager.getInstance()