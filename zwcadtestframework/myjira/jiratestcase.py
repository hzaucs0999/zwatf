from re import S
import requests
import json
import pandas as pd
import os
import yaml
import time
from model.resultstatus import ResultStatus


class jiratestcase:
    jira_server_url = 'http://192.168.56.130:8080/'
    jira_test_api = 'rest/raven/1.0/api/test/'
    jira_testexec_api = 'rest/raven/1.0/api/testexec/'
    jira_issue_api = 'rest/api/2/issue/'
    jira_import_result_api = 'rest/raven/1.0/import/execution/'
    jira_testcase_list = []
    testcase_csv_cols = ['testcasename', 'testcasecode', 'action', 'inputparam', 'outputparamname', 'outputparampath', 'assertMsg', 'assertExpect', 'assertActual']
    testcase_csv_dir = 'D:\\autotest\jira'
    testcase_csv_file = testcase_csv_dir + '\\testcase.csv'
    pre_yaml_file = testcase_csv_dir + '\\project.yaml'
    testsuite_yaml_file = testcase_csv_dir + '\\entry.yaml'
    payload={}
    headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Basic YWRtaW46YWRtaW4xMjM=',
  # 'Cookie': 'JSESSIONID=754866F26F5523249BA033DC39130AB4; atlassian.xsrf.token=B9ZG-4Q42-06KM-VDT4|137f3344e00132cb3558c2403653dcfc69198a35|lin'
}
    __init__flag = True
    
    @classmethod
    def getInstance(cls):
        if hasattr(cls,"_instance"):
            return cls._instance
        else:
            return None

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance'):
            orig = super(jiratestcase, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance
    
    def __init__(self, testexeckey) -> None:
        if self.__init__flag:
            testkeylist = []
            testkeylist = self.getTestexec(testexeckey)
            for tk in testkeylist:
                testcase = self.getTeststeps(tk)
                print("testcase:", testcase)
                if not os.path.exists(self.testcase_csv_file):
                    pd.DataFrame(testcase).to_csv(self.testcase_csv_file, header=self.testcase_csv_cols, index=False,mode='a+' )
                else:
                    pd.DataFrame(testcase).to_csv(self.testcase_csv_file, header=False, index=False,mode='a+' )
            self.getPrecondition(testexeckey)
            self.getTestsuite(testexeckey)
            self.__init__flag = False

    def getTestsuite(self, testexeckey):
        entry = {"task":{"name":"task","suites":[{"name":"","case":[]}]}}
        entry['task']['suites'][0]['name'] = testexeckey
        entry['task']['suites'][0]['case'].append('testcase.csv')
        # print("entry: ", entry)
        # 写入ymal文件
        with open(self.testsuite_yaml_file, "w",encoding='utf-8') as f:
            yaml.dump(entry, f)

    def getPrecondition(self, testexeckey) -> None:
        project = {"projectconfig":{"task":{},"environment":{}}}
        project_json = {}
        url = self.jira_server_url + self.jira_issue_api + testexeckey
        # print("jira_issue_url:", url)
        response = requests.request("GET", url, headers=self.headers, data=self.payload)
        precondition = response.json()['fields']['customfield_10304']
        # print("resp:", precondition)
        project_json = yaml.safe_load(precondition)
        # 判断前提条件的内容是否为字典
        if isinstance(project_json, dict) == False:
           project['projectconfig']['task'] = ''
           project['projectconfig']['environment'] = ''
        else:
        #    print("project_json:", project_json)
           project['projectconfig']['task']['whenerror'] = project_json.get('whenerror', '')
           project['projectconfig']['environment']['domain'] = project_json.get('domain', '')
           project['projectconfig']['environment']['protocal'] = project_json.get('protocal', '')
           project['projectconfig']['environment']['browsertype'] = project_json.get('browsertype', '')
           project['projectconfig']['environment']['headless'] = project_json.get('headless', '')
           project['projectconfig']['environment']['excutepath'] = project_json.get('excutepath', '')
        # print("project:", project)
        # 写入ymal文件
        with open(self.pre_yaml_file, "w",encoding='utf-8') as f:
            yaml.dump(project, f)


    def getTestexec(self, testexeckey):
        url = self.jira_server_url + self.jira_testexec_api + testexeckey +'/test'
        response = requests.request("GET", url, headers=self.headers, data=self.payload)
        jira_testkey_list = []
        for tk in json.loads(response.text):
            if tk['status'] == 'TODO':
               jira_testkey_list.append(tk['key'])
        if len(jira_testkey_list) == 0:
            raise Exception(r'不存在待执行的测试用例')
        return jira_testkey_list

    def getTeststeps(self, testkey):
        testcase = []
        url = self.jira_server_url + self.jira_test_api + testkey + '/step'
        response = requests.request("GET", url, headers=self.headers, data=self.payload)

        if response.status_code != 200:
               raise Exception(r'连接异常')
        for step in response.json():
            step_list = {}
            # 测试步骤序号
            # step_list['stepindex'] = step['index']
            # 测试用例名称
            step_list['testcasename'] = testkey
            # 测试用例编号
            step_list['testcasecode'] = testkey
            # 测试用例关键字
            step_list['action'] = step['step']['raw']
            # 测试用例的输入数据
            step_data = yaml.safe_load(step['data']['raw'])
            # print("step_data:",type(step_data), step_data)
            # 判断用例的输入数据是否为字典
            if not isinstance(step_data, dict) :
               step_list['inputparam'] = ''
            else:
                step_input = ''
                # print("step_data:", step_data)
                for k,v in step_data.items():
                    step_input = step_input + k + '@@' + str(v) +"|"
                step_input=step_input[:-1]
                # print("step_input: ", step_input)
                step_list['inputparam'] = step_input
            step_result = yaml.safe_load(step['result']['raw'])
            # 判断测试用例的结果内容是否为字典
            if not isinstance(step_result, dict):
               step_list['outputparamname'] = ''
               step_list['outputparampath'] = ''
               step_list['assertMsg'] = ''
               step_list['assertExpect'] = ''
               step_list['assertActual'] = ''
            else:  
                # 测试用例的输出参数的名称                
                # print("step_result: " , step_result)
                step_list['outputparamname'] = step_result.get('outputparamname','')
                # 测试用例的输出参数的匹配路径
                step_list['outputparampath'] = step_result.get('outputparampath','')
                # 测试用例被断言的返回信息
                step_list['assertMsg'] = step_result.get('assertMsg','')
                # 测试用例被断言的返回信息的预期值
                step_list['assertExpect'] = step_result.get('assertExpect','')
                # 测试用例被断言的返回信息的实际值
                step_list['assertActual'] = step_result.get('assertActual','')
            testcase.append(step_list)
        # reverselist = sorted(self.step_list,key=lambda x:x["stepindex"])
        # print("reverse_list:",reverselist)
        # print('step_list:',self.step_list)
        return testcase
    
    def setTestResult(self,testexeckey, caseresult) -> None:
        payload = {"testExecutionKey":"","info":{"user":"admin"},"tests":[]}
        payload['testExecutionKey'] = testexeckey
        testresult = {}
        testresult['testKey'] = caseresult._code
        if caseresult.getExcuteResult() == ResultStatus.success:
           testresult['status'] = "PASS"
        else:
           testresult['status'] = "FAIL"
        # testresult['comment'] = caseresult.__duration
        payload['tests'].append(testresult)
        # print("payload:", payload)
        url = self.jira_server_url + self.jira_import_result_api
        # print("import url:", url)
        # print("headers:", self.headers)
        response = requests.request("POST", url, headers=self.headers, data=json.dumps(payload))
        # print("import result resp:", response.text)

    def isjson(self, raw_msg):    
    # 用于判断一个字符串是否符合Json格式    
        if isinstance(raw_msg, str):  # 首先判断变量是否为字符串
            try:
                json.loads(raw_msg)
            except ValueError:
                return False
            return True
        else:
            return False
    
  
# steps = jiratestcase(testexeckey='ZCX-6')

