import logging  # 引入logging模块


class logger():
    __init__flag = True

    @classmethod
    def getinstance(cls):
        return cls._instance

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance'):
            orig = super(logger, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance

    def __init__(self):
        if self.__init__flag:
            self.logger = logging.getLogger()
            self.logger.setLevel(logging.DEBUG)
            self.formatter = logging.Formatter(
                "%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
            # 建立输出流
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)  # 输出到console的log等级的开关
            ch.setFormatter(self.formatter)
            self.logger.addHandler(ch)
            self.__init__flag = False

    def setFileHandler(self, logpath):
        # 移除所有filehandler
        for handler in self.logger.handlers:
            if type(handler) == logging.FileHandler:
                self.logger.removeHandler(handler)
        fh = logging.FileHandler(logpath, mode='w', encoding="utf-8")
        fh.setLevel(logging.DEBUG)  # 输出到file的log等级的开关
        # 第三步，定义handler的输出格式
        fh.setFormatter(self.formatter)
        self.logger.addHandler(fh)

    @staticmethod
    def warn(msg):
        logger.getinstance().logger.warning(msg)

    @staticmethod
    def info(msg):
        logger.getinstance().logger.info(msg)

    @staticmethod
    def error(msg):
        logger.getinstance().logger.error(msg)

    @staticmethod
    def debug(msg):
        logger.getinstance().logger.debug(msg)
