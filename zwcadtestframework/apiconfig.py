winuiapi = {
    "startprogram": "openProgram", "clickwin": "click", "typekeyswin": "type_keys",
    "winimagepatternclick": "getControlPosbyImagePattern","switchDialog":"switchDialog","maximum_size":"maximum_size","win_user_screen_shot":"user_screen_shot",
    "findcontrol":"findcontrol","findcontrolfuzzymatch":"findcontrolfuzzymatch","startnewProgram":"startnewProgram"
    ,"getcontrolPositionwin":"getcontrolPosition","print_control_identifiers":"print_control_identifiers","menuSelect":"menu_select","windowText":"getwindowText"
}
webuiapi = {
    "openbrowser": "openbrowser", "filltext": "fill", "click": "click", "switchpage": "switchPage",
    "uploadfile": "upload", "gethtmlvalue": "getHtmlvalue", "webimagepatternclick": "getControlPosbyImagePattern"
    ,"web_user_screen_shot":"user_screen_shot"
}
requestsapi = {
    "requests": "excute"
}

wsapi = {
    "connect": "createConnection", "sendmsg": "sendMsg", "recivemsg": "reciveMsg"
}

mouseapi = {
    "mousemoveTo": "moveTo", "mousemove": "move", "mouseclick": "mouseclick", "dragTo": "dragTo", "drag": "drag",
    "mouseDown": "mouseDown", "mouseUp": "mouseUp", "scroll": "scroll"
}
keyboardapi = {
    "press": "press", "hotkeys": "hotkeys", "keydown": "keydown", "keyup": "keyup","presscmdln":"presscmdln",
    "presscmd":"presscmd"
}

scriptapi={
    "doscript":"excute"
}
toolapi={"randint":"randomInt","randstr":"randomstr","readcsvLineData":"readcsvLineData","readexcelLineData":"readexcelLineData"
         ,"stradd":"stradd","getClipData":"getClipData"}
