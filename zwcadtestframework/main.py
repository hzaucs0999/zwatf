"""
    入口函数


"""
import os.path
import click
from model.testtask import testtask
from resourcepath import resource_path
from decorator import ui_decorator,ws_decorator,inf_decorator
from myjira.jiratestcase import *
@click.command()
@click.option('--path', default='', help='执行的excel路径,可以为空,为空则执行所有用例')
@click.option('--version', default='', help='执行项目版本')
@click.option('--tekey', default='', help = 'jira测试执行的key')
def main(path=None,version=None,tekey=None):
    if path:
        print("version", version)
        if not os.path.isdir(path):
            raise Exception("传入必须时文件夹")
        task = testtask(path, version)
        task.runTask()
    elif tekey:
         if not os.path.isdir(jiratestcase(tekey).testcase_csv_dir):
            raise Exception("测试用例路径异常")
         task = testtask(jiratestcase(tekey).testcase_csv_dir, version)
         task.runTask()
    





if __name__=="__main__":
    # main(r"D:\工作\云事业部\4_project\zwcadcloudtestplatform\3_project\testcase")
    main()
