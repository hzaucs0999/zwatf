"""
   hook管理

"""


from pluggy import PluginManager
# hook_spec = HookspecMarker("zwcloudtestframework")
# hook_impl = HookimplMarker("zwcloudtestframework")

class hookmanager():
    __init_flag=True
    @classmethod
    def getInstance(cls):
        if not hasattr(cls, '_instance') or not cls._instance:
            orig = super(hookmanager, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance') or not cls._instance:
            orig = super(hookmanager, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance

    def __init__(self):
        if self.__init_flag:
            self.pm=PluginManager("zwcloudtestframework")

            self.__init_flag=False

    def register_hookspec(self,spec):
        self.pm.add_hookspecs(spec)

    def register_hookimpl(self,impl):
        self.pm.register(impl)

    def run_impl(self,implname,*args,**kwargs):
        if hasattr(self.pm.hook,implname):
            func=getattr(self.pm.hook,implname)
            return func(*args,**kwargs)
        else:
            raise Exception("%s未定义的实现"%(implname))




