"""
    项目配置信息存储
"""
import enum

pathmap = {"chrome": "chromebroswerpath", "firefox": "firefoxbroswerpath", "webkit": "webkitbroswerpath"}


class ReportType(enum.Enum):
    html = "html"
    csv = "csv"
    excel = "excel"


class TaskStopType(enum.Enum):
    stoptask = "stoptask"
    continues = "continue"
    stoptestsuites = "stoptestsuites"


class BrowserType(enum.Enum):
    chrome = "chrome"
    firefox = "firefox"
    webkit = "webkit"


class ReportConfig(object):
    def __init__(self, cfg):
        self.type = ReportType.html
        self.runenvironment = True
        self.summary = True


class TaskCfg(object):
    def __init__(self, cfg):
        whenerror = cfg.get("whenerror", "stoptestsuites")
        if hasattr(TaskStopType, whenerror):
            self.whenerror = TaskStopType[whenerror]
        else:
            # 如果没有则为停止测试集执行
            self.whenerror = TaskStopType.stoptestsuites

    def getwhenerror(self):
        return self.whenerror.value


class EnvironmentCfg(object):
    def __init__(self, envfg):
        self.domain = envfg.get("domain", None)
        self.protocal = envfg.get("protocal", None)
        browsertype = envfg.get("browsertype", "chrome")
        if hasattr(BrowserType, browsertype):
            self.browsertype = BrowserType[browsertype]
        else:
            # 如果不存在，则以谷歌启动
            self.browsertype = BrowserType.chrome
        self.headless = envfg.get("headless", False)
        self.excutepath = envfg.get("excutepath", None)
        self.chromebroswerpath = envfg.get("chromebroswerpath", None)
        self.firefoxbroswerpath = envfg.get("firefoxbroswerpath", None)
        self.webkitbroswerpath = envfg.get("webkitbroswerpath", None)

    def getbrowserpath(self):
        """
           返回当前的浏览器配置地址
        """
        return getattr(self, pathmap[self.browsertype.value])


class MailCfg(object):
    def __init__(self, cfg):
        self.senderaddr = cfg.get("senderaddr", None)
        self.senderpwd = cfg.get("senderpwd", None)
        self.recivers = cfg.get("recivers", None)
        self.smtpurl = cfg.get("smtpurl", None)
        self.smtpport = cfg.get("smtpport", None)
        self.smtpprotocal = cfg.get("smtpprotocal", None)


class ProjectCfg(object):

    def __init__(self, cfg):
        """
            配置环境信息
        :param cfg:
        """
        self.cfg = cfg
        # self.report = ReportConfig(cfg["report"])
        self.task = TaskCfg(cfg["task"])
        environmentcfg = cfg["environment"]
        self.environment = EnvironmentCfg(environmentcfg)
        # self.mail = MailCfg(cfg["mail"])

    def getWhenError(self):
        return self.task.whenerror

    def getBrowsetpath(self):
        return self.environment.getbrowserpath()

    def getBrowsettype(self):
        return self.environment.browsertype.value
