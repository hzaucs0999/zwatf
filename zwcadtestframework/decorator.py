"""
    功能:装饰器
"""
import traceback
from log.log import logger
from functools import wraps
from model.processresult import ProcessResult
from model.resultstatus import ResultStatus
from common.getcontext import getTestSuiteContext, getContext
from common.toolutil import replace, is_json
import time
import re


def ui_decorator(a_func):
    @wraps(a_func)
    def make_decorator(self, inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result=result
        # result["errorimageShot"] = []
        logger.info("开始执行ui操作%s,%s" % (a_func.__name__, inputparam))
        starttime = time.time()
        try:
            kwargs = getMethodParam(inputparam, a_func, var)
            a_func(self, **kwargs)
            result.setexcuteresult(ResultStatus.success)
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            imagepath = getTestSuiteContext().screenshot(var["cur_action"])
            #
            result.adderrorimageshot(imagepath)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("执行ui操作%s结束" % (a_func.__name__))
        logger.info("执行ui操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator


def inf_decorator(a_func):
    @wraps(a_func)
    def make_decorator(self, inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result = result
        logger.info("开始执行Inf操作%s" % (a_func.__name__))
        starttime = time.time()
        try:
            infresult = doInfMethodParam(self, inputparam, a_func, var)
            result.setinfresult(infresult)
            result.setexcuteresult(ResultStatus.success)
            var["status_code"] = infresult["statu_code"]  # 压入状态码
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("开始执行Inf操作%s结束" % (a_func.__name__))
        logger.info("开始执行Inf操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator


def doInfMethodParam(self, inputparam, func, var):
    """
       将inputparam中的信息付给infinfo后进行调用
    :param inputparam:
    :param var:
    :return:
    """
    args = inputparam.split("|")
    interfacecfg = getContext().getInstance().interface
    # 首位参数默认为infname,key为infname
    argsmap = {}
    argsmap["key"] = {}
    argsmap["paramlist"] = []
    for arg in args:
        arg = replace(arg, var)
        if "@@" in arg:
            # 则可能有key
            key = arg.split("@@")[0]
            value = arg.split("@@")[1]
            argsmap["key"][key] = value
        else:
            argsmap["paramlist"].append(arg)
    if "infname" in argsmap["key"]:
        infname = argsmap["key"]["infname"]
    else:
        if len(argsmap["paramlist"]) != 0:
            infname = argsmap["paramlist"][0]
    if infname:
        headers = {}
        data = {}
        jsondata = {}
        paramsdata = {}
        files = {}
        cfg = interfacecfg[infname]
        method = cfg["request"]["method"]
        api_url = cfg["request"]["apiurl"]
        responsetype = cfg["reponse"]["type"]
        idx = 1
        if "param" in cfg["request"]:
            # 有参数才做参数提取
            params = cfg["request"]["param"]
            for paramname in params:
                position = params[paramname]["position"]
                if position == "header":
                    if paramname in argsmap["key"]:
                        headers[paramname] = argsmap["key"][paramname]
                    else:
                        if idx < len(argsmap["paramlist"]):
                            headers[paramname] = argsmap["paramlist"][idx]
                            idx = idx + 1
                elif position == "form":
                    if paramname in argsmap["key"]:
                        data[paramname] = argsmap["key"][paramname]
                    else:
                        if idx < len(argsmap["paramlist"]):
                            data[paramname] = argsmap["paramlist"][idx]
                            idx = idx + 1
                        else:
                            data[paramname] = ""
                elif position == "querystring":
                    if paramname in argsmap["key"]:
                        paramsdata[paramname] = argsmap["key"][paramname]
                    else:
                        if idx < len(argsmap["paramlist"]):
                            paramsdata[paramname] = argsmap["paramlist"][idx]
                            idx = idx + 1

                elif position == "json":
                    if paramname in argsmap["key"]:
                        jsondata[paramname] = argsmap["key"][paramname]
                    else:
                        if idx < len(argsmap["paramlist"]):
                            jsondata[paramname] = argsmap["paramlist"][idx]
                            idx = idx + 1
                        else:
                            jsondata[paramname] = ""
                elif position == "file":
                    if paramname in argsmap["key"]:
                        files[paramname] = open(argsmap["key"][paramname], "rb")
                    else:
                        if idx < len(argsmap["paramlist"]):
                            files[paramname] = open(argsmap["paramlist"][idx], "rb")
                            idx = idx + 1

                elif position == "path":
                    pattern = "\$\{([^$]*)\}"
                    paramnamelst = re.findall(pattern, api_url, flags=0)
                    for paramname in paramnamelst:
                        if paramname in argsmap["key"]:
                            api_url = api_url.replace('${%s}' % (paramname), argsmap["key"]["paramname"])
                        else:
                            if idx < len(argsmap["paramlist"]):
                                api_url = api_url.replace('${%s}' % (paramname), argsmap["paramlist"][idx])
                                idx = idx + 1
        logger.info("请求头:" + str(headers))
        logger.info("表单:" + str(data))
        logger.info("querystring:" + str(paramsdata))
        logger.info("json:" + str(jsondata))
        logger.info("file:" + str(files))
        logger.info("apiurl:" + api_url)
        return func(self, method=method, api_url=api_url, headers=headers, files=files, json=jsondata, data=data,
                    params=paramsdata, responsetype=responsetype)

    else:
        logger.error("无法获取接口名称,无法执行接口")


def getMethodParam(inputparam, func, var):
    if not inputparam:
        return {}
    args = str(inputparam).split("|")
    kwargs = {}
    argsmap = {}
    argsmap["key"] = {}
    argsmap["paramlist"] = []
    for arg in args:
        arg = replace(arg, var)
        if "@@" in arg:
            # 则可能有key
            key = arg.split("@@")[0]
            value = arg.split("@@")[1]
            argsmap["key"][key] = value
        else:
            argsmap["paramlist"].append(arg)

    idx = 0
    for varname in func.__code__.co_varnames:
        if varname == "self":
            continue
        if varname in argsmap["key"]:
            kwargs[varname] = argsmap["key"][varname]
        else:
            if len(argsmap["paramlist"]) > idx:
                kwargs[varname] = argsmap["paramlist"][idx]
                idx = idx + 1
    return kwargs


def ws_decorator(a_func):
    @wraps(a_func)
    def make_decorator(self, inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result = result
        logger.info("开始执行websocket操作%s,%s" % (a_func.__name__, inputparam))
        starttime = time.time()
        try:
            kwargs = getMethodParam(inputparam, a_func, var)
            content = a_func(self, **kwargs)
            if content:
                content = content.replace("'", "\"")
                if is_json(content):
                    result.setinfresult({"content": content, "type": "json"})
                else:
                    result.setinfresult({"content": content, "type": "text"})
            result.setexcuteresult(ResultStatus.success)
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("执行websocket操作%s结束" % (a_func.__name__))
        logger.info("执行websocket操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator


def mousekeyboard_decorator(a_func):
    @wraps(a_func)
    def make_decorator(self, inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result = result
        logger.info("开始执行mousekeyboard操作%s,%s" % (a_func.__name__, inputparam))
        starttime = time.time()
        try:
            kwargs = getMethodParam(inputparam, a_func, var)
            a_func(self, **kwargs)
            result.setexcuteresult(ResultStatus.success)
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("执行mousekeyboard操作%s结束" % (a_func.__name__))
        logger.info("执行mousekeyboard操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator


def script_decorator(a_func):
    @wraps(a_func)
    def make_decorator(self, inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result = result
        logger.info("开始执行script操作%s,%s" % (a_func.__name__, inputparam))
        starttime = time.time()
        try:
            kwargs = getMethodParam(inputparam, a_func, var)
            a_func(self, **kwargs)
            result.setexcuteresult(ResultStatus.success)
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("执行script操作%s结束" % (a_func.__name__))
        logger.info("执行script操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator


def getToolMethodParam(inputparam, func, var):
    if not inputparam:
        return {}
    args = str(inputparam).split("|")
    if len(args) == 1:
        return {}
    kwargs = {}
    argsmap = {}
    argsmap["key"] = {}
    argsmap["paramlist"] = []
    args = args[1:]
    for arg in args:
        arg = replace(arg, var)
        if "@@" in arg:
            # 则可能有key
            key = arg.split("@@")[0]
            value = arg.split("@@")[1]
            argsmap["key"][key] = value
        else:
            argsmap["paramlist"].append(arg)

    idx = 0
    for varname in func.__code__.co_varnames:
        if varname == "self":
            continue
        if varname in argsmap["key"]:
            kwargs[varname] = argsmap["key"][varname]
        else:
            if len(argsmap["paramlist"]) > idx:
                kwargs[varname] = argsmap["paramlist"][idx]
                idx = idx + 1
    return kwargs


def tool_decorator(a_func):
    @wraps(a_func)
    def make_decorator(inputparam, var):
        result = ProcessResult(var["cur_action"])
        getTestSuiteContext().cur_process_result = result
        logger.info("开始执行工具类操作%s,%s" % (a_func.__name__, inputparam))
        starttime = time.time()
        try:
            kwargs = getToolMethodParam(inputparam, a_func, var)
            content = a_func(**kwargs)
            getTestSuiteContext().params[inputparam.split("|")[0]] = content
            logger.info("执行结果:" + str(content))
            result.setexcuteresult(ResultStatus.success)
        except Exception as e:
            result.setexcuteresult(ResultStatus.failed)
            result.seterrorinfo(e.args[0])
            logger.error("执行失败，失败原因为%s" % (traceback.format_exc()))
        endtime = time.time()
        result.setDuration(endtime - starttime)
        logger.info("执行工具类操作%s结束" % (a_func.__name__))
        logger.info("执行工具类操作%s时间%f" % (a_func.__name__, endtime - starttime))
        return result

    return make_decorator
