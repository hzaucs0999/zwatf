"""
   hook报告工具类
"""
from hook import hook_impl
from common.getcontext import getTestSuiteContext
import bs4
import shutil
import os
from model.resultstatus import ResultStatus
from resourcepath import resource_path

class Report(object):
    __init__flag=True

    def __new__(cls, *args, **kwargs):
        """
           单列模式
        """
        if not hasattr(cls, '_instance') or not cls._instance:
            orig = super(Report, cls)
            cls._instance = orig.__new__(cls)
        return cls._instance


    def __init__(self):
        if self.__init__flag:
            templatepath = resource_path(os.path.join("template","report.html"))
            self.reporttemplate = bs4.BeautifulSoup(open(templatepath, "r", encoding="utf-8").read(), features="html.parser")
            self.table = self.reporttemplate.find("table", {"id": "results-table"})
            self.timebanner = self.reporttemplate.select("body > p:nth-child(3)")[0]
            self.__init__flag=False




    @hook_impl
    def report_html_results_table_row(self,caseResult):
        """
          根据testcaseResult报告插入结果
        :param testcaseResult: 测试用列结果
        :return:
        """

        isPass = "Passed"
        if caseResult.getExcuteResult() == ResultStatus.failed:
            tbody_tag = self.reporttemplate.new_tag("tbody", attrs={"class": "failed results-table-row"})
            isPass = "Failed"
        else:
            tbody_tag = self.reporttemplate.new_tag("tbody", attrs={"class": "passed results-table-row"})
        self.table.append(tbody_tag)
        tr_tag = self.reporttemplate.new_tag("tr")
        tbody_tag.append(tr_tag)
        # 添加extra节点
        tr_extra_tag = self.reporttemplate.new_tag("tr",attrs={"class":"collapsed"})
        tbody_tag.append(tr_extra_tag)
        # 添加结果位点
        td_result_tag = self.reporttemplate.new_tag("td", attrs={"class": "col-result"})
        td_result_tag.string = isPass
        td_description_tag = self.reporttemplate.new_tag("td")
        td_description_tag.string = getTestSuiteContext().getTestSuitename()
        td_name_tag = self.reporttemplate.new_tag("td", attrs={"class": "col-name"})
        td_name_tag.string = caseResult.getcode()+"_"+caseResult.getname()
        td_duration_tag = self.reporttemplate.new_tag("td", attrs={"class": "col-duration"})
        td_duration_tag.string = str(round(caseResult.getDuration(),2))
        td_assert_tag = self.reporttemplate.new_tag("td")
        td_detail_tag = self.reporttemplate.new_tag("td")
        td_usercapture_tag = self.reporttemplate.new_tag("td")
        idx = 1
        # 添加extra内容:
        extra_td_tag = self.reporttemplate.new_tag("td", attrs={"class": "extra", "colspan": "6"})
        extra_extra_tag = self.reporttemplate.new_tag("div")
        extra_div_tag = self.reporttemplate.new_tag("div", attrs={"class": "log"})
        with open(caseResult.getLogPath(), "r", encoding="utf-8") as file:
            content = file.read()
        extra_div_tag.string = content  # 获取当前testcase所有输出内容
        for action in caseResult.getActionResult():
            process_tag = self.reporttemplate.new_tag("p")
            process_tag.string="关键字%s,执行时间%s s"%(action.getaction(),str(round(action.getDuration(),2)))
            td_detail_tag.append(process_tag)
            if action.geterrorimageshot():
                for imagepath in action.geterrorimageshot():
                    # base64data = imageUtils.convertBase64(imagepath)
                    imagedivTag = self.reporttemplate.new_tag("div")
                    imgtag = self.reporttemplate.new_tag("img", attrs={"data-src": imagepath,
                                                                  "alt": "screenshot"
                        , "style": "width:600px;height:300px;", "onclick": "window.open(this.src)", "align": "right"})
                    imagedivTag.append(imgtag)
                    extra_extra_tag.append(imagedivTag)
            if action.getassertresult():
                for assertresult in action.getassertresult():
                    p_tag = self.reporttemplate.new_tag("p")
                    p_tag.string = "%d.验证点%s,执行结果为%s,实际值%s,期望值%s" % (
                    idx, assertresult["msg"], assertresult["assertresult"], assertresult["actual"],
                    assertresult["expect"])
                    idx = idx + 1
                    td_assert_tag.append(p_tag)
            if action.getusercapture():
                for user_capture in action.getusercapture():
                     user_capture_p_tag = self.reporttemplate.new_tag("p")
                     user_capture_p_tag.string=user_capture
                     td_usercapture_tag.append(user_capture_p_tag)

        #增加用户截图显示位置
        td_links_tag = self.reporttemplate.new_tag("td", attrs={"class": "col-links"})
        td_links_tag.string = ""
        tr_tag.append(td_result_tag)
        tr_tag.append(td_description_tag)
        tr_tag.append(td_name_tag)
        tr_tag.append(td_duration_tag)
        tr_tag.append(td_assert_tag)
        tr_tag.append(td_detail_tag)
        tr_tag.append(td_usercapture_tag)
        tr_tag.append(td_links_tag)
        extra_td_tag.append(extra_extra_tag)
        extra_td_tag.append(extra_div_tag)
        tr_extra_tag.append(extra_td_tag)

    @hook_impl
    def report_summary(self,passcount,unpasscount,totalcount,fullduration):
        # 修改summary信息
        totalraninfo = self.reporttemplate.select("body > p:nth-child(7)")[0]
        totalraninfo.string = "%d tests ran in %s seconds." % (totalcount, str(round(fullduration,2)))
        passcounttag = self.reporttemplate.select("body > span.passed")[0]
        passcounttag.string = "%d passed" % (passcount)
        unpasscounttag = self.reporttemplate.select("body > span.failed")[0]
        unpasscounttag.string = "%d failed" % (unpasscount)
        # summarytable=self.reporttemplate.select("#results-table1")
        percent=round((passcount/totalcount)*100,2)
        percenttd=self.reporttemplate.select_one("#results-table1 > tr > td:nth-child(1)")
        percenttd.string=str(percent)+"%"
        totaltd = self.reporttemplate.select_one("#results-table1 >  tr > td:nth-child(3)")
        totaltd.string = str(totalcount)
        durationtd = self.reporttemplate.select_one("#results-table1 >  tr > td:nth-child(4)")
        durationtd.string = str(round(fullduration,2))
        passtd = self.reporttemplate.select_one("#results-table1 >  tr > td:nth-child(5)")
        passtd.string = str(passcount)
        failtd = self.reporttemplate.select_one("#results-table1 >  tr > td:nth-child(6)")
        failtd.string =str( unpasscount)




    @hook_impl
    def report_header(self,name):
        headername=self.reporttemplate.select_one("body > h1")
        headername.string=name

    @hook_impl
    def report_Environment(self,Environment):
        print("开始执行")

    @hook_impl
    def save(self,path):
        # 拷贝template下的文件到result目录
        source_path = resource_path(os.path.join("template","assets"))
        target_path = os.path.dirname(path) + os.sep + "assets"
        if not os.path.exists(target_path):
            # 如果目标路径不存在原文件夹的话就创建
            os.makedirs(target_path)

        if os.path.exists(source_path):
            # 如果目标路径存在原文件夹的话就先删除
            shutil.rmtree(target_path)
        shutil.copytree(source_path, target_path)
        with open(path, "w", encoding="utf-8") as fp:
            fp.write(self.reporttemplate.prettify())

    @hook_impl
    def report_start_time(self,time):
        self.timebanner.string = "Report generated on %s by" % (time)
