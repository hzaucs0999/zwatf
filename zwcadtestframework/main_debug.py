"""
    入口函数


"""
import os.path
from model.testtask import testtask
from myjira.jiratestcase import *
def main(path=None,version=None,tekey=None):
    if path:
        print("version", version)
        if not os.path.isdir(path):
            raise Exception("传入必须时文件夹")
        task = testtask(path, version)
        task.runTask()
    elif tekey:
         if not os.path.isdir(jiratestcase(tekey).testcase_csv_dir):
            raise Exception("测试用例路径异常")
         task = testtask(jiratestcase(tekey).testcase_csv_dir, version)
         task.runTask()


if __name__=="__main__":
    # main(r"D:\工作\云事业部\7.自动化测试用例\登录接口","3213")
    # main()
    main(tekey='ZCX-6',version='111')