"""
    功能：定义testcase模型
    author：pengchen
    time:2021/11/20
"""
from model.process import process
from common.getcontext import *
from log.log import logger
import os
from model.testcaseresult import testcaseResult
from model.resultstatus import ResultStatus
from hook import hookmanager
from myjira.jiratestcase import *



class testcase(object):
    def __init__(self, name, code):
        """

        """
        self.name = name
        self.code = code
        self.actionlist = []

    def addaction(self, actiondata):
        """
           actiondata:Series
        """
        cur_process = process(actiondata["action"], actiondata["inputparam"], actiondata["outputparamname"], actiondata["outputparampath"],
                              actiondata["assertMsg"], actiondata["assertExpect"], actiondata["assertActual"])
        self.actionlist.append(cur_process)



    def excute(self):
        """ 执行testcase"""
        #初始化filehandler,避免记录到相同日志
        context=getTestSuiteContext()
        caseresult= testcaseResult(self.name,self.code)
        projectdir=context.getcurpath()
        # actionresult=[]
        logpath=projectdir
        if not os.path.exists(logpath):
            os.makedirs(logpath)
        logger.getinstance().setFileHandler(logpath+os.sep+self.name+"_"+self.code+".log")
        caseresult.setLogpath(logpath+os.sep+self.name+"_"+self.code+".log")
        for cur_process in self.actionlist:
            result=cur_process.excute()
            # actionresult.append(result)
            caseresult.addActionResult(result)
            caseresult.modifyduration(result.getDuration())
            if result.getexcuteresult()=="pok":
                caseresult.setExcuteResult(ResultStatus.failed)
                #根据配置内容做执行判断
                break
            else:
                result.addassertresult(None)
                caseresult.setExcuteResult(ResultStatus.success)
        #传到报告后台执行
        hookmanager().getInstance().run_impl("report_html_results_table_row",caseResult=caseresult)
        # 测试用例执行结果同步给jira
        if  jiratestcase.getInstance():   
            jiratestcase.getInstance().setTestResult(context.getTestSuitename(), caseresult)
        return caseresult
