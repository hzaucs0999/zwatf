"""
   记录测试任务结果工具类
"""


class TaskResult(object):
    def __init__(self, name):
        self._name = name #任务名称
        self._passcount = 0 #任务通过数量
        self._failedcount = 0 #任务失败数量
        self._totalcount = 0 #任务总数量
        self._testsuiteResult = [] # 测试集执行结果
        self._duration = 0 #执行时间
        self._starttime = None #执行开始时间

    def getSummary(self):
        return {"passcount": self._passcount, "failedcount": self._failedcount, "totalcount": self._totalcount}

    def addDuration(self, duration):
        self._duration = self._duration + duration

    def addTestSuiteResult(self, testsuiteResult):
        summary = testsuiteResult.getSummary()
        self._passcount = self._passcount + summary["passcount"]
        self._failedcount = self._failedcount + summary["failedcount"]
        self._totalcount = self._totalcount + summary["totalcount"]
        self._duration = self._duration + testsuiteResult.getDuration()
        self._testsuiteResult.append(testsuiteResult)

    def getTestSuiteResult(self):
        return self._testsuiteResult

    def getDuration(self):
        return self._duration

    def setStartTime(self, starttime):
        self._starttime = starttime

    def getStartTime(self):
        return self._starttime
