"""
   存储testcase的执行结果
"""
from model.resultstatus import ResultStatus

class testcaseResult(object):


    def __init__(self,name,code):
        self._name=name
        self._code=code
        self._excuteresult = ResultStatus.success
        self._duration = 0
        self._actionresultlst = []
        self._logpath = None

    def setLogpath(self,logpath):
        self._logpath=logpath


    def addActionResult(self,actionresult):
        self._actionresultlst.append(actionresult)

    def modifyduration(self,durationvar):
        self._duration=self._duration+durationvar

    def setExcuteResult(self,result):
        self._excuteresult=result

    def getExcuteResult(self):
        return self._excuteresult

    def getActionResult(self):
        return self._actionresultlst

    def getLogPath(self):
        return self._logpath

    def getDuration(self):
        return self._duration

    def getname(self):
        return self._name

    def getcode(self):
        return self._code

