"""
   func:测试集模型
"""
from model.testcase import testcase
from common.getcontext import getTestSuiteContext,getContext
from log.log import logger
from model.testsuiteresult import testsuiteResult
from context import TestSuiteContextManager
from model.resultstatus import ResultStatus

class testsuite():
    def __init__(self,name):
        self.name=name
        self.testcaselist=[]

    def addtestcase(self,testcasedata):
        """
            读取testcasedata:dataframe中的数据
        """
        if testcasedata.shape[0]==0:
            #若为空则直接返回不做执行用例
            return
        cur_test_case_code=testcasedata["testcasecode"].values[0]
        cur_test_case_name=testcasedata["testcasename"].values[0]
        cur_testcase=testcase(cur_test_case_name,cur_test_case_code)
        for index,line in testcasedata.iterrows():
            if line["testcasecode"] and line["testcasecode"]!=cur_test_case_code:
                cur_test_case_code=line["testcasecode"]
                cur_test_case_name = line["testcasename"]
                self.testcaselist.append(cur_testcase)
                cur_testcase=testcase(cur_test_case_name,cur_test_case_code)
            cur_testcase.addaction(line)
        self.testcaselist.append(cur_testcase)

    def prepareEnvironment(self):
        """
          初始化环境
          1.初始化webui
        """
        logger()
        TestSuiteContextManager(self)
        getTestSuiteContext().setTestsuite(self.name)






    def excute(self):
        testsuiteresult=testsuiteResult(self.name)
        self.prepareEnvironment()
        for case in self.testcaselist:
            #执行用例
            getTestSuiteContext().setTestCase((case.name,case.code))
            testcaseresult=case.excute()
            testsuiteresult.addTestCaseResult(testcaseresult)
            if testcaseresult.getExcuteResult()==ResultStatus.failed:
                if getContext().projectcfg.task.getwhenerror()=="stoptask":
                    testsuiteResult.stopflag = True
                    break
                elif getContext().projectcfg.task.getwhenerror()=="stoptestsuites":
                    break


        self.postEnvironment()
        return testsuiteresult


    def postEnvironment(self):
        # self.var["utils"].close()
        getTestSuiteContext().close()
        getTestSuiteContext().closeInstance()
