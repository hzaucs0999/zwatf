"""
   功能：动作类

"""
from log.log import logger
from common.valueutil import *
from common.toolutil import replace,is_number
from model.resultstatus import ResultStatus
from common.getcontext import getTestSuiteContext
import re,time,os

class process(object):
    def __init__(self,action,inputparam,outputparamname=None,outputparampath=None,assertmsg=[],assertexpect=[],assertActual=[]):
        """

        """
        self.action=action
        self.inputparam=inputparam
        self.outputparamname=outputparamname
        self.outputparampath=outputparampath
        self.assertmsg=str(assertmsg)
        self.assertExpect=str(assertexpect)
        self.assertactual=str(assertActual)


    def excute(self):
        """ 执行process"""
        #1.通过action关键字提取
        logger.info("开始提取关键字%s"%(self.action))
        method=getTestSuiteContext().getAction(self.action)
        if not method:
            raise Exception("%s关键字不存在"%(self.action))
        #2.inputparam以|符号隔开
        # 关键字替换
        var = getTestSuiteContext().params
        var["cur_action"]=self.action
        result=method(self.inputparam,var)
        logger.info("关键字%s执行结果%s" % (self.action,result))
        logger.info("关键字%s执行结束"%(self.action))
        #3 解析outputparam,并存入var中
        var["last_sample_status"] = result.getexcuteresult()
        self.pushvalue(result,var)
        #4解析验证点并执行验证点结果
        assertresultlst,assertstatus=self.excuteAssert(var,result)
        result.addassertresult(assertresultlst)
        if assertstatus:
            result.setexcuteresult(assertstatus)
            var["last_sample_status"] = result.getexcuteresult()
        return result

    def excuteAssert(self,var,processresult):
        if self.assertmsg:
            msglist=self.assertmsg.split("|")
            actuallist=self.assertactual.split("|")
            expectlist=self.assertExpect.split("|")
            #对actual对象做
            actuallist= [replace(actual,var) for actual in actuallist]
            assertresult=[]
            assertstatus=ResultStatus.success
            idx=0
            for msg in msglist:
                result={}
                result["msg"]=msg
                if is_number(actuallist[idx]):
                    result["actual"]=float(actuallist[idx])
                    result["expect"] = float(expectlist[idx])
                else:
                    result["actual"] = actuallist[idx]
                    result["expect"] = expectlist[idx]
                if result["actual"]==result["expect"]:
                   result["assertresult"]=True
                else:
                    logger.error("验证点%s执行失败,实际值%s,期望值%s"%(msg,actuallist[idx],expectlist[idx]))
                    result["assertresult"]=False
                    if processresult.geterrorimageshot():
                        imagepath=getTestSuiteContext().screenshot(self.action)
                        processresult.adderrorimageshot(imagepath)
                    assertstatus=ResultStatus.failed
                assertresult.append(result)
                idx=idx+1
            return assertresult,assertstatus
        else:
            return None,None


    def pushvalue(self,result,var):
        """
           后端接口action才可能成功，否则会失败
        :param result:包含content key
        :param var:
        :return:
        """
        if result.getinfresult():
            ouputparamnamelist=self.outputparamname.split("|")
            outputparampathlist=self.outputparampath.split("|")
            if len(ouputparamnamelist)!=len(outputparampathlist):
                raise Exception("输出参数名称和输出参数路径长度不一致")
            idx=0
            for paramname in ouputparamnamelist:
                if paramname:
                    if result.getinfresult()["type"]=="json":
                       var[paramname]=getjsonvalue(result.getinfresult()["content"],outputparampathlist[idx])
                    elif result.getinfresult()["type"]=="html":
                        var[paramname] = gethtmlvalue(result.getinfresult()["content"], outputparampathlist[idx])
                    else:
                        #如果是text类型时,直接返回所有数据，不做特殊处理
                        var[paramname] = result.getinfresult()["content"]
                    idx=idx+1















