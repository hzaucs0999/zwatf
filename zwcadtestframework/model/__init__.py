"""
    定义对象
    包含关系 testtask->testsuite->testcase->process
    包含process 步骤对象
    testcase 测试用例对象
    testcaseresult 测试用例执行结果
    processresult 步骤执行结果
    testsuite 测试集对象
    testsuiteresult 测试集结果
    testtask 测试任务对象
    testtaskresult 测试任务结果
    ResultStatus: enum对象，定义结果类型OK,POK等

"""

