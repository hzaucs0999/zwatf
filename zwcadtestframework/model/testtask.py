"""
   func:测试任务模型
"""
from common.toolutil import readcsv
from model.testsuite import testsuite
import os
from context import contextManger
from model.taskresult import TaskResult
from report import Report
from hook import hookmanager
class testtask():
    def __init__(self,exctuepath,ver):
        contextManger(exctuepath)
        self.result= {}
        self.ver=ver
        self.name=contextManger.getInstance().task["task"]["name"]
        self.testsuitelist=[]
        for suite in contextManger.getInstance().task["task"]["suites"]:
            tsuite=testsuite(suite["name"])
            for case in suite["case"]:
               testcasepath =contextManger.getInstance().projectdir + os.sep + case
               testcasedata=readcsv(testcasepath)
               #数据清洗，避免nan
               testcasedata.fillna("",inplace=True)
               #所有列都为string类型,避免出现1.0的情况
               tsuite.addtestcase(testcasedata)
            self.testsuitelist.append(tsuite)


    def runTask(self):
        #设置任务开始时间
        #生成报告模块
        hm=hookmanager()
        hm.register_hookimpl(Report())
        #修改report头
        hm.run_impl("report_header",name=self.name)
        taskresult=TaskResult(self.name)
        taskresult.setStartTime(contextManger.getInstance().starttime)
        hm.run_impl("report_start_time",time=taskresult.getStartTime())
        for suite in self.testsuitelist:
            testsuiteresult=suite.excute()
            taskresult.addTestSuiteResult(testsuiteresult)
            if testsuiteresult.stopflag==True:
                break
        summary=taskresult.getSummary()
        hm.run_impl("report_summary", passcount=summary["passcount"],unpasscount=summary["failedcount"],totalcount=summary["totalcount"],fullduration=taskresult.getDuration())
        projectdir = contextManger.getInstance().getResultpath()
        #加上当前时间,并用后缀区分
        reportpath = projectdir +os.sep+ "report"+"_"+str(self.ver)+".html"
        hm.run_impl("save",path=reportpath)

        # #暂时增加，预计报告模块做可定制化解耦
        # totalcount,passcount,unpasscount=report(self.result).to_html(reportpath)
        # #cicd文件
        #cicd文件
        cicdpath= contextManger.getInstance().projectdir + os.sep+"result"+os.sep+".result"
        with open(cicdpath,"w",encoding="utf-8") as file:
            file.writelines([self.name+"\n",reportpath+"\n",str(summary["failedcount"])+"\n"])

        # #发送邮件
        # mailclent = smtpclient()
        # mailclent.sendmail("总共执行用例%d个，成功%d个，失败%d个" % (summary["totalcount"], summary["passcount"], summary["failedcount"]))





