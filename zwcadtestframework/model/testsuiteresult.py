"""
   存储测试集的结果
"""
from model.resultstatus import ResultStatus


class testsuiteResult(object):

    def __init__(self, name):
        self._name = name
        self._passcount = 0
        self._failedcount = 0
        self._totalcount = 0
        self._testcaseResult = []
        self._duration = 0
        self.stopflag=False

    def getSummary(self):
        return {"passcount": self._passcount, "failedcount": self._failedcount, "totalcount": self._totalcount}

    def addTestCaseResult(self, testcaseResult):
        excuteresult = testcaseResult.getExcuteResult()
        if excuteresult == ResultStatus.failed:
            self._failedcount = self._failedcount + 1
        else:
            self._passcount = self._passcount + 1
        self._totalcount = self._totalcount + 1
        self._duration = self._duration + testcaseResult.getDuration()
        self._testcaseResult.append(testcaseResult)

    def getTestCaseResult(self):
        return self._testcaseResult

    def getDuration(self):
        return self._duration
