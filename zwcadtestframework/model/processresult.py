"""

    步骤执行结果
"""
from model.resultstatus import ResultStatus


class ProcessResult(object):
    def __init__(self,action):
        self._action=action
        self._assertresult = None  # 步骤断点执行结果
        self._excuteresult = ResultStatus.success  # 步骤执行结果,默认为成功
        self._errorinfo = None  # 步骤错误信息
        self._errorimageshot = []  # 步骤中的错误截图地址
        self._duration = 0  # 步骤执行时间
        self._infresult=None
        self._usercapture=[]

    def setDuration(self, duration):
        self._duration = duration

    def getDuration(self):
        return self._duration

    def addassertresult(self, assertresult):
        self._assertresult=assertresult

    def getassertresult(self):
        return self._assertresult

    def seterrorinfo(self, errinfo):
        self._errorinfo = errinfo

    def geterrorinfo(self):
        return self._errorinfo

    def adderrorimageshot(self, errorimage):
        self._errorimageshot.append(errorimage)

    def geterrorimageshot(self):
        return self._errorimageshot

    def addcaptureimageshot(self, capture):
        self._usercapture.append(capture)

    def getusercapture(self):
        return self._usercapture


    def setexcuteresult(self, excuteresult):
        self._excuteresult = excuteresult

    def getexcuteresult(self):
        return self._excuteresult.value

    def setinfresult(self, infresult):
        self._infresult = infresult

    def getinfresult(self):
        return self._infresult

    def getaction(self):
        return self._action

    def gatherAttrs(self):
        return ",".join("{}={}"
                        .format(k, getattr(self, k))
                        for k in self.__dict__.keys())
    def __str__(self):
        return "[{}:{}]".format(self.__class__.__name__, self.gatherAttrs())

